// test3.cpp : Defines the entry point for the console application.
//
//#define DEBUG

#include "stdafx.h"
#include <iostream>
#include <smmintrin.h>
#include <stdio.h>
#include <string.h> /* for memcpy */
#include <algorithm> //std:sort
#include <cstdlib>.
#include "ipp.h"
using namespace std;

#define NUM_OF_TESTS 100000
#define ARRAY_LENGTH 64
static float a[ARRAY_LENGTH];
static float b[ARRAY_LENGTH];
static float res[ARRAY_LENGTH];

#define min(x,y) ( (x) > (y) ? (y) : (x) )
#include <cstdlib> //rand
#include <ctime> //time

#ifdef 		_WIN32					//  Windows
#define 	cpuid    __cpuid
#else								//  Linux
#include <uintstd.h>/* for sysconf */
void cpuid(int CPUInfo[4], int InfoType) {
	__asm__ __volatile__(
		"cpuid":
		"=a" (CPUInfo[0]),
		"=b" (CPUInfo[1]),
		"=c" (CPUInfo[2]),
		"=d" (CPUInfo[3]) :
		"a" (InfoType)
		);
}
#endif


static uint32_t l2_cache_size = 0;


inline void vector_cmpswap(__m128 *in, int min_idx, int max_idx)
{
	__m128 tmp = _mm_min_ps(in[min_idx], in[max_idx]);
	in[max_idx] = _mm_max_ps(in[min_idx], in[max_idx]);
	in[min_idx] = tmp;
}
inline void vector_cmpswap_skew(__m128 *in, int min_idx, int max_idx)
{
	__m128 x =_mm_castsi128_ps (_mm_slli_si128(_mm_castps_si128(in[min_idx]), 4)); // shift left -> {in[min_idx][1 -3], 0}
	__m128 y = _mm_min_ps(in[max_idx], x); // y = { min(in[min_idx][1],in[max_idx][0]), min(in[min_idx][2],in[max_idx][1]), ...,min(0,in[max_idx][2])}
	in[max_idx] = _mm_max_ps(in[max_idx], x); 
	in[min_idx] = _mm_castsi128_ps( _mm_srli_si128(_mm_castps_si128(in[min_idx]), 12));
	in[min_idx] = _mm_castsi128_ps(_mm_alignr_epi8(_mm_castps_si128(in[min_idx]), _mm_castps_si128(y), 4));
}

//step1-1
inline void sort4vec(__m128* in)
{
	
	__m128 min01 = _mm_min_ps(in[0], in[1]);
	__m128 max01 = _mm_max_ps(in[0], in[1]);
	__m128 min23 = _mm_min_ps(in[2], in[3]);
	__m128 max23 = _mm_max_ps(in[2], in[3]);
	in[0] = _mm_min_ps(min01, min23);
	in[3] = _mm_max_ps(max01, max23);
	__m128 maxofmin = _mm_max_ps(min01, min23);
	__m128 minofmax = _mm_min_ps(max01, max23);
	in[1] = _mm_min_ps(maxofmin, minofmax);
	in[2] = _mm_max_ps(maxofmin, minofmax);
}

//step1
inline void sort_each_vector(__m128* in, uint32_t n)
{
	for (int i = 0; i < n; i+= 4)
	{
		sort4vec(&in[i]);
		_MM_TRANSPOSE4_PS(in[i], in[i+1], in[i+2], in[i+3]);
	}
}




inline size_t nextGap(size_t n)
{
	n = (n * 10) / 13;
	// Comb sort magic number :D
	// Combsort11. http://cs.clackamas.cc.or.us/molatore/cs260Spr03/combsort.htm
	if (n == 9 || n == 10) return 11;
	return n;
}


inline bool isSortedVec(__m128* in, uint32_t n) 
{
	for (size_t i = 0; i < n - 1; i++) {
		__m128 a = in[i];
		__m128 b = in[i + 1];
		__m128 c = _mm_max_ps(a, b);
		c = _mm_sub_ps(c, b);
		// a <= b <=> max(a, b) == b => b - max(a,b) = 0
		if (!_mm_testz_si128(_mm_castps_si128(c), _mm_castps_si128(c)))
			return false;
	}
	return true;
}
//step2
inline bool SIMD_combsort(__m128*in, uint32_t n)
{
	size_t gap = nextGap(n);
	while (gap > 1) {
		for (size_t i = 0; i < n - gap; i++) {
			vector_cmpswap(in, i, i + gap);
		}
		for (size_t i = n - gap; i < n; i++) {
			vector_cmpswap_skew(in, i, i + gap - n);
		}
		gap = nextGap(gap);
	}

	const int maxLoop = 10;
	for (int i = 0; i < maxLoop; i++) {
		__m128 a = in[0];
		__m128 b = in[1];
		in[0] = _mm_min_ps(a, b);
		a = _mm_max_ps(a, b);
		for (size_t i = 1; i < n - 1; i++) {
			b = in[i + 1];
			__m128 t = _mm_max_ps(a, b);
			in[i] = _mm_min_ps(a, b);
			a = t;
		}
		in[n - 1] = a;
		vector_cmpswap_skew(in, n - 1, 0);
		if (isSortedVec(in, n)) {
			return true;
		}

	}
	return false;
}

inline void reorder_data(__m128 *in, __m128 *res, uint32_t n)
{
	for (size_t i = 0; i < n; i+=4 ) {
		_MM_TRANSPOSE4_PS(in[i], in[i + 1], in[i + 2], in[i + 3]);
	}
	for (size_t i = 0; i < 4; i++) {
		for (size_t j = 0; j < n / 4; j++) {
			res[j + i * (n / 4)] = in[j * 4 + i];
		}
	}
}




inline void vectorized_comb_sort(__m128* in, __m128* out, uint32_t n)
{
#ifdef DEBUG
	cout << " in vectorized comb sort and n = " << n << endl;
#endif
	//step1
	sort_each_vector(in,n);

#ifdef DEBUG
	for (int i = 0; i < n; i++)
	{
		float* tmp = (float*) &in[i];
		if (!(tmp[0] <= tmp[1] && tmp[1] <= tmp[2] && tmp[2] <= tmp[3]))
			cout << "first step error! at i: " <<i<< endl;
	}

#endif

	//step2
	bool is_sorted = SIMD_combsort(in, n);
	//step3
	reorder_data(in, out, n);

	if (!is_sorted)
	{
		sort((float*) (&out[0]), (float*) (&out[n]));
#ifdef DEBUG
		cout << "Hell no! std sort needed!" << endl;
#endif
	}
}

void init_array()
{
	srand(time(0));
	for (size_t i = 0; i < ARRAY_LENGTH; i++)
	{
		a[i] = (float)(rand() + rand()) / RAND_MAX * rand();
		b[i] = a[i];
	}
}

int main()
{
#if ARRAY_LENGTH > 65536
#ifdef _WIN32
	int info[4];
	cpuid(info, 0x80000006);
	l2_cache_size = (info[2] & 0xFF);
	l2_cache_size *= 1024; // kilo byte -> byte
#else
	l2_cache_size = (uint32_t)sysconf(_SC_LEVEL2_CACHE_SIZE);
#endif
	
	cout << "L2 cache size: " << l2_cache_size / 1024 << "KB" << endl;
	uint32_t block_size = l2_cache_size / 2; // block size in bytes
	uint32_t block_elements = block_size / 4; // num of block's floating points 
#endif

	Ipp64u start, end;
	Ipp64u parallel_time =0, sequential_time=0;
	for (size_t testnum = 0; testnum < NUM_OF_TESTS; testnum++)
	{

	
		init_array();
		start = ippGetCpuClocks();
		uint32_t  this_block_elements;
		uint32_t i = 0;

	#if ARRAY_LENGTH > 65536
		for (; i < ARRAY_LENGTH; i += block_elements) {
			this_block_elements = min(ARRAY_LENGTH - i, block_elements);
	#else 
		this_block_elements = ARRAY_LENGTH;
	#endif
			vectorized_comb_sort((__m128*) &a[i], (__m128*) &res[i], this_block_elements/4);
			memcpy(a, res, this_block_elements * sizeof(a[0]));
	#if ARRAY_LENGTH > 65536
		}
		vectorize_block_merge(block_size);
	#endif
		end = ippGetCpuClocks();
		parallel_time += end - start;
	#ifdef DEBUG
		bool not_sorted = false;
		for (size_t i = 0; i < ARRAY_LENGTH-1; i++)
		{
			if (a[i] > a[i + 1]) {
				not_sorted = true;

				cout << "at the end i :" << i << " is more than the next one" << endl;
			}
		}
		if (not_sorted)
			cout << "It's not fully sorted!" << endl;
		else
	#endif
		//cout << "Congrats! It's fully sorted in parallel in " <<parallel_time << " clocks"<<endl;
	
		start = ippGetCpuClocks();
		sort(&b[0], &b[ARRAY_LENGTH]);
		end = ippGetCpuClocks();
		sequential_time = sequential_time - start + end;

		//cout << "Sequential sort clock count is " << sequential_time << endl;
	}
	cout << "The speed up is " << (double)sequential_time / parallel_time << endl;
    return 0;
}



/* 
inline void aasort_vector_transpose(__m128 *xf) {
__m128 tf[4];
tf[0] = _mm_unpacklo_ps(xf[0], xf[2]);
tf[1] = _mm_unpacklo_ps(xf[1], xf[3]);
tf[2] = _mm_unpackhi_ps(xf[0], xf[2]);
tf[3] = _mm_unpackhi_ps(xf[1], xf[3]);
xf[0] = _mm_unpacklo_ps(tf[0], tf[1]);
xf[1] = _mm_unpackhi_ps(tf[0], tf[1]);
xf[2] = _mm_unpacklo_ps(tf[2], tf[3]);
xf[3] = _mm_unpackhi_ps(tf[2], tf[3]);
}

*/
