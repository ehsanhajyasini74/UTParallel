#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <climits>
#include <iostream>
#include <ipp.h>

using namespace cv;
using namespace std;
#define SOBEL_OP_SIZE 9
#define IMAGE_DEPTH CV_8U

#define PARAM_W 3
#define PARAM_B 45
#define PARAM_W2 PARAM_W/2



unsigned int mat_diff(Mat& left, Mat& right, int x, int y, int k) {
  unsigned int res =0;
  int width = right.size().width;
  for(int i = y - PARAM_W2; i <= y + PARAM_W2 ; i++) {
    for(int j = x- PARAM_W2; j <= x + PARAM_W2; j++) {
      res += abs((int)right.data[i*width+j] - (int)left.data[i*width + j+k]);
    }
  }
  return res;
}

int find_min_k(Mat& left, Mat& right, int x, int y) {
    unsigned int min_k = 0, min_val = UINT_MAX;

    for(int k = 0; k <= PARAM_B; k++) {
      int diff = mat_diff(left, right, x, y, k);
      if (diff < min_val) {
        min_val = diff;
        min_k = k;
      }
    }
    return min_k;
}
inline unsigned char scale_8bit(int val) {
  // return val;
  return val * 255.0 / PARAM_B;
}

Mat stereo_depth(Mat& left, Mat& right) {
    Mat res(left.size(), IMAGE_DEPTH);
    int width = right.size().width, height= right.size().height;
    for(int i = PARAM_W2; i < height -  PARAM_W2; i++) {
      for(int j =  PARAM_W2; j < width -  PARAM_W2 - PARAM_B; j++) {
         res.data[i*width +j] = scale_8bit(find_min_k(left, right, j, i));
        //res.data[i*width +j] = find_min_k(left, right, j, i) * 255.0/PARAM_B;
      }
    }
    return res;
}

/** @function main */
int main( int argc, char** argv )
{
  Mat src_left, src_right, src_left_gray, src_right_gray;
  Mat depth;
  char* window_name = "Stereo Vision Depth Demo";
  int scale = 1;
  int delta = 0;

  int c;

  /// Load an image
  src_left = imread( argv[1], CV_LOAD_IMAGE_GRAYSCALE );
  src_right = imread( argv[2] , CV_LOAD_IMAGE_GRAYSCALE);
  if( !src_left.data || !src_right.data )
  { return -1; }

  /// Convert it to gray
  // cvtColor( src_left, src_left_gray, COLOR_RGB2GRAY );
  // cvtColor( src_right, src_right_gray, COLOR_RGB2GRAY );
  /// Create window
  namedWindow( window_name, WINDOW_AUTOSIZE );
  Ipp64u start, end;
  start = ippGetCpuClocks();

  // depth = stereo_depth(src_left_gray, src_right_gray);
  depth = stereo_depth(src_left, src_right);

  end   = ippGetCpuClocks();
  cerr << "parallel time\n";
  cerr << (Ipp64s) (end - start) << endl;

  imshow( window_name, depth );
  imwrite("v1.bmp", depth);
  waitKey(0);

  return 0;
}
