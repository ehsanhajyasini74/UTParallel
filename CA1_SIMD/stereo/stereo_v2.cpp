#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <climits>
#include <cstdint>
#include <iostream>
#include <ipp.h>

using namespace cv;
using namespace std;
#define SIMD_STEP 8
#define IMAGE_DEPTH CV_8U

#define PARAM_W 3
#define PARAM_B 45
#define PARAM_W2 PARAM_W/2
#define PARAM_B_SCALE (255.0 / PARAM_B)
const __m128i beta_scale_factor = _mm_set1_epi16(PARAM_B_SCALE);

Mat stereo_depth_parallel(Mat& left, Mat& right) {
    Mat res(left.size(), IMAGE_DEPTH);
    int width = right.size().width, height= right.size().height;
    int8_t *buffer = (int8_t *) _mm_malloc(sizeof(int8_t)*16, 16); //to store results

    for(int i = PARAM_W2; i < height -  PARAM_W2; i++) {
      for(int j =  PARAM_W2; j < width -  PARAM_W2 - PARAM_B; j+=8) {
        __m128i min_k = _mm_set1_epi16(PARAM_B);
        __m128i min_val = _mm_set1_epi16(INT16_MAX);
        for(int k = 0; k <= PARAM_B; k++) {
          __m128i sum = _mm_setzero_si128();
          for(int y = i - PARAM_W2; y <= i + PARAM_W2 ; y++) {
            for(int x = j- PARAM_W2; x <= j + PARAM_W2; x++) {
              __m128i right_data = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(right.data + y*width+x)), _mm_setzero_si128());
              __m128i left_data = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(left.data + y*width+x+k)), _mm_setzero_si128());
              sum = _mm_add_epi16(sum,_mm_abs_epi16(_mm_sub_epi16(right_data, left_data)));
            }
          }

          //update minimum
          __m128i min_mask = _mm_cmplt_epi16(sum, min_val);
          min_val =_mm_min_epi16(sum, min_val);

          __m128i curr_k = _mm_set1_epi16(k);
          curr_k = _mm_and_si128(min_mask, curr_k);
          min_k = _mm_andnot_si128(min_mask, min_k);
          min_k = _mm_or_si128(min_k, curr_k);

          // min_k = _mm_or_si128(_mm_andnot_si128(min_mask, min_k),_mm_and_si128(min_mask, _mm_set1_epi16(k)) );
        }
        //store min
        min_k = _mm_mullo_epi16(beta_scale_factor, min_k);
        _mm_store_si128((__m128i*) buffer,
                    _mm_packs_epi16(min_k, _mm_setzero_si128() ));
        memcpy((int8_t *)res.data + (width * i + j), buffer, SIMD_STEP);
      }
    }
    _mm_free(buffer);
    return res;
}

/** @function main */
int main( int argc, char** argv )
{
  Mat src_left, src_right, src_left_gray, src_right_gray;
  Mat depth;
  char* window_name = "Stereo Vision Depth Demo";
  int scale = 1;
  int delta = 0;

  int c;

  /// Load an image
  src_left = imread( argv[1], CV_LOAD_IMAGE_GRAYSCALE );
  src_right = imread( argv[2] , CV_LOAD_IMAGE_GRAYSCALE);
  if( !src_left.data || !src_right.data )
  { return -1; }

  /// Convert it to gray
  // cvtColor( src_left, src_left_gray, COLOR_RGB2GRAY );
  // cvtColor( src_right, src_right_gray, COLOR_RGB2GRAY );
  /// Create window
  namedWindow( window_name, WINDOW_AUTOSIZE );
  Ipp64u start, end;
  start = ippGetCpuClocks();

  // depth = stereo_depth(src_left_gray, src_right_gray);
  depth = stereo_depth_parallel(src_left, src_right);

  end   = ippGetCpuClocks();
  cerr << "parallel time\n";
  cerr << (Ipp64s) (end - start) << endl;

  imshow( window_name, depth );
  imwrite("v2.bmp", depth);
  waitKey(0);

  return 0;
}
