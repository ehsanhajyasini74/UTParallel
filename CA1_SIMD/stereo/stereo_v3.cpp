#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <climits>
#include <cstdint>
#include <iostream>
#include <ipp.h>

using namespace cv;
using namespace std;
#define SIMD_STEP 8
#define IMAGE_DEPTH CV_8U

#define PARAM_W 3
#define PARAM_B 45
#define PARAM_W2 PARAM_W/2
#define PARAM_B_SCALE (255.0 / PARAM_B)
const __m128i beta_scale_factor = _mm_set1_epi16(PARAM_B_SCALE);
// __m128i zero_arr[PARAM_B] __attribute__ ((aligned (16)));
inline void set_zero_arr(__m128i* m) {
  memset(m , (unsigned char)0, sizeof(__m128i) * PARAM_B);
}
inline void load_right_data(__m128i* right_data, Mat& right, int width, int y, int j) {
  for(int i = 0; i <= PARAM_W; i++)
  {
    right_data[i] = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(right.data + y*width+(j-PARAM_W2+i))), _mm_setzero_si128());
  }
}
Mat stereo_depth_parallel(Mat& left, Mat& right) {
    Mat res(left.size(), IMAGE_DEPTH);
    int width = right.size().width, height= right.size().height;
    int8_t *buffer = (int8_t *) _mm_malloc(sizeof(int8_t)*16, 16); //to store results
    __m128i * sum = (__m128i *) _mm_malloc(sizeof(__m128i)*PARAM_B, 16);
    __m128i * right_data = (__m128i *) _mm_malloc(sizeof(__m128i)*(PARAM_W+1), 16);

    for(int i = PARAM_W2; i < height -  PARAM_W2; i++) {
      for(int j =  PARAM_W2; j < width -  PARAM_W2 - PARAM_B; j+=8) {
        // __m128i sum[PARAM_B] __attribute__ ((aligned (16)));

        for(int y = i - PARAM_W2; y <= i + PARAM_W2 ; y++) {
          set_zero_arr(sum);
          load_right_data(right_data, right, width, y, j);

          for(int k = 0; k <= PARAM_B; k++) {
            for(int x = j- PARAM_W2; x <= j + PARAM_W2; x++) {
              __m128i left_data = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(left.data + y*width+x+k)), _mm_setzero_si128());
              sum[k] = _mm_add_epi16(sum[k],_mm_abs_epi16(_mm_sub_epi16(right_data[(x-j+PARAM_W2)], left_data)));
            }
          }
        }
        //update minimum
        __m128i min_k = _mm_set1_epi16(PARAM_B);
        __m128i min_val = _mm_set1_epi16(INT16_MAX);
        for(int k = 0; k <= PARAM_B; k++) {
          __m128i min_mask = _mm_cmplt_epi16(sum[k], min_val);
          min_val =_mm_min_epi16(sum[k], min_val);

          min_k = _mm_or_si128(_mm_andnot_si128(min_mask, min_k),_mm_and_si128(min_mask, _mm_set1_epi16(k)) );
          // __m128i curr_k = _mm_set1_epi16(k);
          // curr_k = _mm_and_si128(min_mask, curr_k);
          // min_k = _mm_andnot_si128(min_mask, min_k);
          // min_k = _mm_or_si128(min_k, curr_k);
        }
        //store min
        min_k = _mm_mullo_epi16(beta_scale_factor, min_k);
        _mm_store_si128((__m128i*) buffer,
                    _mm_packs_epi16(min_k, _mm_setzero_si128() ));
        memcpy((int8_t *)res.data + (width * i + j), buffer, SIMD_STEP);
      }
    }
    // _mm_free(buffer);
    // _mm_free(sum);
    // _mm_free(right_data);
    return res;
}

/** @function main */
int main( int argc, char** argv )
{
  Mat src_left, src_right, src_left_gray, src_right_gray;
  Mat depth;
  char* window_name = "Stereo Vision Depth Demo";
  int scale = 1;
  int delta = 0;

  int c;
  /// Load an image
  src_left = imread( argv[1], CV_LOAD_IMAGE_GRAYSCALE );
  src_right = imread( argv[2] , CV_LOAD_IMAGE_GRAYSCALE);
  if( !src_left.data || !src_right.data )
  { return -1; }

  /// Convert it to gray
  // cvtColor( src_left, src_left_gray, COLOR_RGB2GRAY );
  // cvtColor( src_right, src_right_gray, COLOR_RGB2GRAY );
  /// Create window
  namedWindow( window_name, WINDOW_AUTOSIZE );
  Ipp64u start, end;
  start = ippGetCpuClocks();

  // depth = stereo_depth(src_left_gray, src_right_gray);
  depth = stereo_depth_parallel(src_left, src_right);

  end   = ippGetCpuClocks();
  cerr << "parallel time\n";
  cerr << (Ipp64s) (end - start) << endl;

  imshow( window_name, depth );
  imwrite("v3.bmp", depth);
  waitKey(0);

  return 0;
}
