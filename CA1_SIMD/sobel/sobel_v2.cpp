#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <ipp.h>

using namespace cv;
using namespace std;
#define SOBEL_OP_SIZE 9
#define IMAGE_DEPTH CV_8U
/*
 * Make the operation memory for iterative convolution
 */

void makeOpMem(Mat buffer, int c_index, unsigned char* op_mem) {
    int width = buffer.size().width;
    int buffer_size = buffer.total();

    int top = c_index-width < 0;
    int bottom = c_index+width >= buffer_size;
    int left = c_index % width == 0;
    int right = (c_index+1) % width == 0;

    op_mem[6] = !bottom && !left  ? buffer.data[c_index+width-1] : 0;
    op_mem[7] = !bottom           ? buffer.data[c_index+width]   : 0;
    op_mem[8] = !bottom && !right ? buffer.data[c_index+width+1] : 0;

    op_mem[3] = !left             ? buffer.data[c_index-1]       : 0;
    op_mem[4] = buffer.data[c_index];
    op_mem[5] = !right            ? buffer.data[c_index+1]       : 0;

    op_mem[0] = !top && !left     ? buffer.data[c_index-width-1] : 0;
    op_mem[1] = !top              ? buffer.data[c_index-width]   : 0;
    op_mem[2] = !top && !right    ? buffer.data[c_index-width+1] : 0;
}

/*
 * Performs convolution between first two arguments
 */

int convolution(unsigned char* x, int* y, int c_size) {
    int sum = 0;

    for(int i=0; i<c_size; i++) {
        sum += x[i] * y[i];
    }

    return sum;
}

/*
 * Iterate Convolution
 */

Mat itConv(Mat buffer, int *op) {
    // Allocate memory for result
    int buffer_size = buffer.total();
    int width = buffer.size().width;
    Mat res(buffer.size(), IMAGE_DEPTH);
    // Temporary memory for each pixel operation
    unsigned char op_mem[SOBEL_OP_SIZE];
    memset(op_mem, 0, SOBEL_OP_SIZE);


    // Make convolution for every pixel
    for(int i=0; i<buffer_size; i++) {
        // Make op_mem
        makeOpMem(buffer,i, op_mem);

        // Convolution
        res.data[i] = (unsigned char) abs(convolution(op_mem, op, SOBEL_OP_SIZE));

        /*
         * The abs function is used in here to avoid storing negative numbers
         * in a byte data type array. It wouldn't make a different if the negative
         * value was to be stored because the next time it is used the value is
         * squared.
         */
    }
    return res;
}

Mat sobelFilter(Mat gray) {
    int sobel_h[] = {-1, 0, 1, -2, 0, 2, -1, 0, 1},
        sobel_v[] = {1, 2, 1, 0, 0, 0, -1, -2, -1};
    Mat res(gray.size(), IMAGE_DEPTH);
    // Make sobel operations
    Mat sobel_h_res = itConv(gray, sobel_h);
    Mat sobel_v_res = itConv(gray, sobel_v);
    return sobel_v_res;

    // Calculate contour matrix
    addWeighted( sobel_h_res, 0.5, sobel_v_res, 0.5, 0, res );
    return res;
}


/** @function main */
int main( int argc, char** argv )
{
  Mat src, src_gray;
  Mat grad;
  char* window_name = "Sobel Demo - Simple Edge Detector";
  int scale = 1;
  int delta = 0;
  // int ddepth = IMAGE_DEPTH;

  int c;

  /// Load an image
  src = imread( argv[1] );

  if( !src.data )
  { return -1; }

  /// Convert it to gray
  cvtColor( src, src_gray, COLOR_RGB2GRAY );

  /// Create window
  namedWindow( window_name, WINDOW_AUTOSIZE );
  Ipp64u start, end;
  start = ippGetCpuClocks();
  grad = sobelFilter(src_gray);
  end   = ippGetCpuClocks();
  cerr << "parallel time\n";
  cerr << (Ipp32s) (end - start) << endl;

  //imshow( window_name, grad );
  imwrite("v2.bmp", grad);
  //waitKey(0);

  return 0;
}
