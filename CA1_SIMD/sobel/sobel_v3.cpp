#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace cv;
using namespace std;
#define SOBEL_OP_SIZE 9
#define IMAGE_DEPTH CV_8U
#define SIMD_STEP 8

#include <ipp.h>
#include <x86intrin.h>
const __m128i factor2 = _mm_set1_epi16 (2);

inline void sobel_row(int8_t* src_data, int8_t* buffer, int width) {//buffer should be 16byte aligned
    __m128i top[3], bottom[3];
    top[0] = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(src_data-width-1)), _mm_setzero_si128());
    top[1] = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(src_data-width)), _mm_setzero_si128());
    top[2] = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(src_data-width+1)), _mm_setzero_si128());

    bottom[0] = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(src_data+width-1)), _mm_setzero_si128());
    bottom[1] = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(src_data+width)), _mm_setzero_si128());
    bottom[2] = _mm_unpacklo_epi8(_mm_loadu_si128((__m128i*)(src_data+width+1)), _mm_setzero_si128());

    top[1] = _mm_slli_epi16(top[1], 1);
    bottom[1] = _mm_slli_epi16(bottom[1], 1);
    // top[1] = _mm_mullo_epi16(factor2, top[1]);
    // bottom[1] = _mm_mullo_epi16(factor2, bottom[1]);

    __m128i top_res    = _mm_add_epi16(top[0], _mm_add_epi16(top[1],top[2]));
    __m128i bottom_res = _mm_add_epi16(bottom[0], _mm_add_epi16(bottom[1],bottom[2]));
    // cerr << "x, y, res: [" << x << " " << y <<" {" << hex <<(void*)res <<"}] pos:" <<(void*) res + (width * y + x) << endl;
    _mm_store_si128((__m128i*) buffer,
                    _mm_packs_epi16(_mm_abs_epi16(_mm_sub_epi16(bottom_res, top_res)), _mm_setzero_si128() ));

}

Mat sobel(Mat src) {
  Mat res(src.size(), IMAGE_DEPTH);
  //int16_t buffer[8] __attribute__ ((aligned (16)));
  //cout <<"buffer: "<<sizeof(buffer);
  unsigned char * p_res = res.data;
  int width = src.size().width, height = src.size().height;
  int8_t *buffer = (int8_t *) _mm_malloc(sizeof(int8_t)*16, 16);

  for(int i = 1; i < height -2; i++) {
    for(int j = 1; j < width -1 - SIMD_STEP; j+= SIMD_STEP) {
      sobel_row(((int8_t*)src.data) + (i*width + j), buffer, width);
      memcpy((int8_t *)res.data + (width * i + j), buffer, SIMD_STEP);
    }
  }
  _mm_free(buffer);
  return res;
}
Mat sobelFilterSIMD(Mat src) {
  Mat sobel_h, sobel_v, res;
  sobel_h = sobel(src);
  sobel_v = sobel(src.t()).t();
  addWeighted( sobel_h, 0.5, sobel_v, 0.5, 0, res );
  return res;
  // res = sobel_h + sobel_v;
}

int main( int argc, char** argv )
{
  Mat src, src_gray;
  Mat grad;
  char* window_name = "Sobel Demo - Simple Edge Detector";
  int scale = 1;
  int delta = 0;
  // int ddepth = IMAGE_DEPTH;

  int c;

  /// Load an image
  src = imread( argv[1] );

  if( !src.data )
  { return -1; }

  /// Convert it to gray
  cvtColor( src, src_gray, COLOR_RGB2GRAY );

  /// Create window
  namedWindow( window_name, WINDOW_AUTOSIZE );
  Ipp64u start, end;
  start = ippGetCpuClocks();
  grad = sobelFilterSIMD(src_gray);
  end   = ippGetCpuClocks();
  cerr << "parallel time\n";
  cerr << (Ipp32s) (end - start) << endl;
  //printf ("Parallel sobel takes %d cycles\n", (Ipp32s) (end - start));
  imshow( window_name, grad );
  //imwrite("v3.bmp", grad);

  waitKey(0);

  return 0;
}
